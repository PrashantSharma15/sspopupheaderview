//
//  main.m
//  SSPopoverTableView
//
//  Created by James Van Dyne on 12/20/13.
//  Copyright (c) 2013 Sugoi Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SSAppDelegate class]));
    }
}
